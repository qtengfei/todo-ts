import React, { FunctionComponent } from 'react'
import { Todo } from "../../interfaces/Todo";
import './TaskForm.css'

interface Props {
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onAdd: (event: React.FormEvent<HTMLFormElement>) => void;
  todo: Todo;
}

const TaskForm: FunctionComponent<Props> = ({
  onChange,
  onAdd,
  todo
}) => (
  <form onSubmit={onAdd}>
    <input 
      autoFocus 
      type="text" 
      className="new-todo" 
      onChange={onChange} 
      value={todo.taskName}
      placeholder="What want to do?"
    />
  </form>
);

export default TaskForm
