import React from "react";
import { Todo } from "./../../interfaces/Todo";
import './TodoItem.css';

interface Props {
  todo: Todo;
  onDelete: (todo: Todo) => void;
  onToggleFinished: (id: Todo["id"]) => void;
}

const TodoItem = ({ todo, onDelete, onToggleFinished }: Props) => {
  const onClick = (e: any) => {
    onDelete(todo);
  };

  return (
    <li 
      className="todo-item"
      >
      <input
        className="icon icon-toggle"
        type="checkbox"
        checked={todo.isFinished}
        onChange={() => onToggleFinished(todo.id)}
      />

      {
        todo.isFinished ? (
          <h3 className="text finished">{todo.taskName}</h3>
        ) : (
          <h3 className="text">{todo.taskName}</h3>
        )
      }
      <i className="icon icon-delete" onClick={onClick}>×</i>
    </li>
  );
};

export default TodoItem;