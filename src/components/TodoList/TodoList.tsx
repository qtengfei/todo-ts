import React from "react";
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import TodoItem from "./../TodoItem/TodoItem";
import { Todo } from "./../../interfaces/Todo";
import './TodoList.css';

interface Props {
  todos: Todo[];
  onDelete: (todo: Todo) => void;
  onToggleFinished: (id: Todo["id"]) => void;
}

const TodoList = ({ todos, onDelete, onToggleFinished }: Props) => {
  return (
    <ul className="todo-list">
      <ReactCSSTransitionGroup
        transitionName="item-transition"
        transitionEnterTimeout={500}
        transitionLeaveTimeout={300}
        >
        {todos.map((todo, idx) => (
          <TodoItem
            key={idx}
            onDelete={onDelete}
            onToggleFinished={onToggleFinished}
            todo={{
              id: todo.id,
              taskName: todo.taskName,
              isFinished: todo.isFinished
            }}
          />
        ))}
      </ReactCSSTransitionGroup>
    </ul>
  );
};

export default TodoList;