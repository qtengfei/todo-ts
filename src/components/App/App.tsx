import React, { Component } from "react";
import { Todo } from "../../interfaces/Todo";
import TodoList from "./../TodoList/TodoList";
import TaskForm from "../TaskForm/TaskForm"; 
import './App.css';

// mock ID
let ID = 1;

interface State {
  newTask: Todo;
  todos: Todo[];
}

class App extends Component<{}, State> {
  state = {
    newTask: {
      id: ID,
      taskName: "",
      isFinished: false
    },
    todos: []
  };

  render() {
    let clear
    let hasFishedTask = this.state.todos.some((t: Todo) => t.isFinished)

    if (hasFishedTask) {
      clear = (<div className="clear" onClick={this.onClearFinishedTodo}>Clear</div>)
    }

    return (
      <div className="app-wrap">
        <h1 className="app-name">TO-DO LIST</h1>
        <TaskForm
          todo={this.state.newTask}
          onAdd={this.addTask}
          onChange={this.handleTaskChange}
        />
        <TodoList 
          todos={this.state.todos} 
          onDelete={this.deleteTask}
          onToggleFinished={this.onToggleFinished}
        />
        {clear}
      </div>
    );
  }

  private addTask = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (!this.state.newTask.taskName) return;

    this.setState(previousState => ({
      newTask: {
        id: previousState.newTask.id + 1,
        taskName: "",
        isFinished: false
      },
      todos: [...previousState.todos, previousState.newTask]
    }));
  };

  private onClearFinishedTodo = (event: React.FormEvent<HTMLDivElement>) => {
    event.preventDefault();
    let todos = this.state.todos.slice().filter((t: Todo) => !t.isFinished)

    this.setState({
      todos: todos 
    });
  };

  private onToggleFinished = (todoId: Todo['id']): void => {
    let todos = this.state.todos.slice();
    this.setState({
      todos: todos.map(
        (todo: Todo): Todo => {
          if (todo.id === todoId) {
            return { ...todo, isFinished: !todo.isFinished }
          } else {
            return todo
          }
        }
      )
    })
  }

  private handleTaskChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      newTask: {
        ...this.state.newTask,
        taskName: event.target.value,
        isFinished: false
      }
    });
  };

  private deleteTask = (todoToDelete: Todo) => {
    this.setState(previousState => ({
      todos: [
        ...previousState.todos.filter(todo => todo.id !== todoToDelete.id)
      ]
    }));
  };
}

export default App;