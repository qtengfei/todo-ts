export interface Todo {
  id: number,
  taskName: string;
  isFinished: boolean;
};